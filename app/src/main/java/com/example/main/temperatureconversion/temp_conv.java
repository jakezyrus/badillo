package com.example.main.temperatureconversion;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class temp_conv extends Activity implements OnClickListener{

    Button btnconv;
    TextView inptx, ansshow;
    String a;
    Double b;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp_conv);

        btnconv = (Button) findViewById(R.id.btnconv);
        inptx = (TextView) findViewById(R.id.inptx);
        ansshow = (TextView) findViewById(R.id.ansshow);
        btnconv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        a = inptx.getText().toString();
        b = Double.valueOf(a);
        b = (b * 1.8) + 32;
        a = String.valueOf(b);
        ansshow.setText(a);    }

}

